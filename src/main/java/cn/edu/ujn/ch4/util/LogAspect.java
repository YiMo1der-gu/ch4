package cn.edu.ujn.ch4.util;

import java.util.logging.Logger;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;
@Aspect
@Component
public class LogAspect {

	public static Logger log = Logger.getLogger(LogAspect.class.toString());
	@Pointcut("execution(* cn.edu.ujn.ch4.dao.*.*(..))")
	public void myPointcut() {

	}

	
	public void mybefore(JoinPoint jp) {
		log.info("��־�����" +jp.getTarget()+":"+ jp.getSignature().getName());
	}
	@Around("myPointcut()")
	public Object myAround(ProceedingJoinPoint pjp) throws Throwable{
		log.info("before"+ pjp.getSignature().getName()+"..........");
		Object proceed = pjp.proceed();
		log.info("after"+pjp.getSignature().getName()+".........."+proceed+"...");
		return proceed;
	}
}
