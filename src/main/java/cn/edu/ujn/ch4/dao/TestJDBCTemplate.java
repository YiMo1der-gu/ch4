package cn.edu.ujn.ch4.dao;

import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;

public class TestJDBCTemplate {
	public static void main(String[] args) {
		ClassPathXmlApplicationContext ctx = 
				new ClassPathXmlApplicationContext("applicationContext.xml");
		JdbcTemplate jdbcTemplate = 
				(JdbcTemplate) ctx.getBean("jdbcTemplate");
		jdbcTemplate.execute("create table account(id int primary key auto_increment,username varchar(50),balance double)");
		System.out.println("数据表account创建成功！");
		
	}

}
