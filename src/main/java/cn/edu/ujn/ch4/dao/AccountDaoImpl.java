package cn.edu.ujn.ch4.dao;

import java.sql.PreparedStatement;
import java.util.List;
import java.util.Objects;

import com.mysql.jdbc.Connection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
/**
 * @author lenovo
 */
@Repository
public class AccountDaoImpl implements IAccountDao {
	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Override
	public int addAccount(Account account) {
		// TODO Auto-generated method stub
		System.out.println("addAccount.............");
		KeyHolder holder = new GeneratedKeyHolder();
		String sql = "insert into account(username,balance) value(?,?)";
		int update = this.jdbcTemplate.update(connection->{
			PreparedStatement ps = connection.prepareStatement(sql,new String[]{"id"});
			ps.setString(1,account.getUsername());
			ps.setDouble(2,account.getBalance());
			return ps;
		},holder);
		int id = Objects.requireNonNull(holder.getKey()).intValue();
		return id;
	}

	@Override
	public int updateAccount(Account account) {
		// TODO Auto-generated method stub
		String sql = "update account set username=?,balance=? where id = ?";
		Object obj[]=new Object[] {account.getUsername(),account.getBalance(),account.getId()};
		int update = this.jdbcTemplate.update(sql,obj);
		return update;
	}

	@Override
	public int deleteAccount(int id) {
		// TODO Auto-generated method stub
		String sql = "delete  from account where id = ? ";
		int update = this.jdbcTemplate.update(sql,id);
		return update;
	}

	@Override
	public Account findAccountById(int id) {
		// TODO Auto-generated method stub
		String sql = "select * from account where id = ?";
		BeanPropertyRowMapper<Account> rowMapper = 
				new BeanPropertyRowMapper<Account>(Account.class);
		Account account = this.jdbcTemplate.queryForObject(sql,rowMapper,id);
		return account;
	}

	@Override
	public List<Account> findAllAccount() {
		// TODO Auto-generated method stub

		return null;
	}


}
