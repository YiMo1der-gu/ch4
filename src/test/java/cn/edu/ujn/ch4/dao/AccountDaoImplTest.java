package cn.edu.ujn.ch4.dao; 

import org.junit.Test; 
import org.junit.Before; 
import org.junit.After; 

/** 
* AccountDaoImpl Tester. 
* 
* @author <Authors name> 
* @since <pre>10/22/2021</pre> 
* @version 1.0 
*/ 
public class AccountDaoImplTest { 

    @Before
    public void before() throws Exception { 
    } 
    
    @After
    public void after() throws Exception { 
    } 

    /** 
    * 
    * Method: addAccount(Account account) 
    * 
    */ 
    @Test
    public void testAddAccount() throws Exception { 
    //TODO: Test goes here... 
    } 
    
    /** 
    * 
    * Method: updateAccount(Account account) 
    * 
    */ 
    @Test
    public void testUpdateAccount() throws Exception { 
    //TODO: Test goes here... 
    } 
    
    /** 
    * 
    * Method: deleteAccount(int id) 
    * 
    */ 
    @Test
    public void testDeleteAccount() throws Exception { 
    //TODO: Test goes here... 
    } 
    
    /** 
    * 
    * Method: findAccountById(int id) 
    * 
    */ 
    @Test
    public void testFindAccountById() throws Exception { 
    //TODO: Test goes here... 
    } 
    
    /** 
    * 
    * Method: findAllAccount() 
    * 
    */ 
    @Test
    public void testFindAllAccount() throws Exception { 
    //TODO: Test goes here... 
    } 
    
    
    } 
